# Lab5 -- Integration testing

## Specification

<https://script.google.com/macros/s/AKfycbzp093GuTsC8jWmn6v8zWvaUAw0LErgD-P8VXt71Ll8-y2lewGh0Mw6sFyg6truO66T/exec?action=get&&service=getSpec&email=v.safonov@innopolis.university>

```text
Budet car price per minute = 23
Luxury car price per minute = 57
Fixed price per km = 14
Allowed deviations in % = 17
Inno discount in % = 20
```

## Results

Note: `inno_discount = no` unless stated otherwise

<!-- markdownlint-disable MD033 -->
<table>
    <tr>
        <th>Description</th>
        <th>Request URL</th>
        <th>Expected result</th>
        <th>Obtained result</th>
        <th>Verdict</th>
    </tr>
    <tr>
        <td>distance = -10</td>
        <td>
            <a href="https://script.google.com/macros/s/AKfycbzp093GuTsC8jWmn6v8zWvaUAw0LErgD-P8VXt71Ll8-y2lewGh0Mw6sFyg6truO66T/exec?action=get&&service=calculatePrice&type=luxury&plan=minute&distance=-10&planned_distance=10&time=10&planned_time=10&inno_discount=no&email=v.safonov@innopolis.university">Request</a>
        </td>
        <td>Invalid Request</td>
        <td>Invalid Request</td>
        <td>No error</td>
    </tr>
    <tr>
        <td>
            type = luxury<br>
            plan = minute<br>
            distance = 0<br>
            time = 10
        </td>
        <td>
            <a href="https://script.google.com/macros/s/AKfycbzp093GuTsC8jWmn6v8zWvaUAw0LErgD-P8VXt71Ll8-y2lewGh0Mw6sFyg6truO66T/exec?action=get&&service=calculatePrice&type=luxury&plan=minute&distance=0&planned_distance=10&time=10&planned_time=10&inno_discount=no&email=v.safonov@innopolis.university">Request</a>
        </td>
        <td>Invalid Request</td>
        <td>570</td>
        <td>Domain error, but may be expected behaviour</td>
    </tr>
    <tr>
        <td>
            type = luxury<br>
            plan = minute<br>
            distance = 0<br>
            time = 10<br>
            inno_discount = yes
        </td>
        <td>
            <a href="https://script.google.com/macros/s/AKfycbzp093GuTsC8jWmn6v8zWvaUAw0LErgD-P8VXt71Ll8-y2lewGh0Mw6sFyg6truO66T/exec?action=get&&service=calculatePrice&type=luxury&plan=minute&distance=0&planned_distance=10&time=10&planned_time=10&inno_discount=yes&email=v.safonov@innopolis.university">Request</a>
        </td>
        <td>Invalid Request</td>
        <td>490.2</td>
        <td>
            Domain error<br>
            If behaviour is expected — Computational (discount applied is 14% instead of 20%)
        </td>
    </tr>
    <tr>
        <td>
            type = budget<br>
            plan = fixed_price<br>
            distance = 0<br>
            planned_distance = 10<br>
            time = 10
        </td>
        <td>
            <a href="https://script.google.com/macros/s/AKfycbzp093GuTsC8jWmn6v8zWvaUAw0LErgD-P8VXt71Ll8-y2lewGh0Mw6sFyg6truO66T/exec?action=get&&service=calculatePrice&type=budget&plan=fixed_price&distance=0&planned_distance=10&time=10&planned_time=10&inno_discount=no&email=v.safonov@innopolis.university">Request</a>
        </td>
        <td>Invalid Request</td>
        <td>166.6666</td>
        <td>
            Domain error<br>
            If behaviour is expected to switch to minutes plan — Computational; behaves as if price per minute is 16.666
        </td>
    </tr>
    <tr>
        <td>time = -10</td>
        <td>
            <a href="https://script.google.com/macros/s/AKfycbzp093GuTsC8jWmn6v8zWvaUAw0LErgD-P8VXt71Ll8-y2lewGh0Mw6sFyg6truO66T/exec?action=get&&service=calculatePrice&type=luxury&plan=minute&distance=10&planned_distance=10&time=-10&planned_time=10&inno_discount=no&email=v.safonov@innopolis.university">Request</a>
        </td>
        <td>Invalid Request</td>
        <td>Invalid Request</td>
        <td>No error</td>
    </tr>
    <tr>
        <td>
            plan = minute<br>
            time = 0
        </td>
        <td><a href="https://script.google.com/macros/s/AKfycbzp093GuTsC8jWmn6v8zWvaUAw0LErgD-P8VXt71Ll8-y2lewGh0Mw6sFyg6truO66T/exec?action=get&&service=calculatePrice&type=luxury&plan=minute&distance=10&planned_distance=10&time=0&planned_time=10&inno_discount=no&email=v.safonov@innopolis.university">Request</a></td>
        <td>Invalid Request</td>
        <td>0.0</td>
        <td>Domain error</td>
    </tr>
    <tr>
        <td>
            plan = fixed_price<br>
            time = 0
        </td>
        <td><a href="https://script.google.com/macros/s/AKfycbzp093GuTsC8jWmn6v8zWvaUAw0LErgD-P8VXt71Ll8-y2lewGh0Mw6sFyg6truO66T/exec?action=get&&service=calculatePrice&type=budget&plan=fixed_price&distance=10&planned_distance=10&time=0&planned_time=10&inno_discount=no&email=v.safonov@innopolis.university">Request</a></td>
        <td>Invalid Request</td>
        <td>0.0</td>
        <td>Domain error</td>
    </tr>
    <tr>
        <td>
            plan = fixed_price<br>
            planned_distance = 0
        </td>
        <td><a href="https://script.google.com/macros/s/AKfycbzp093GuTsC8jWmn6v8zWvaUAw0LErgD-P8VXt71Ll8-y2lewGh0Mw6sFyg6truO66T/exec?action=get&&service=calculatePrice&type=budget&plan=fixed_price&distance=10&planned_distance=0&time=10&planned_time=10&inno_discount=no&email=v.safonov@innopolis.university">Request</a></td>
        <td>Invalid Request</td>
        <td>166.6666</td>
        <td>Domain error</td>
    </tr>
    <tr>
        <td>
            plan = minute<br>
            planned_time = 0
        </td>
        <td><a href="https://script.google.com/macros/s/AKfycbzp093GuTsC8jWmn6v8zWvaUAw0LErgD-P8VXt71Ll8-y2lewGh0Mw6sFyg6truO66T/exec?action=get&&service=calculatePrice&type=luxury&plan=minute&distance=10&planned_distance=10&time=10&planned_time=0&inno_discount=no&email=v.safonov@innopolis.university">Request</a></td>
        <td>Invalid Request</td>
        <td>570.0</td>
        <td>Domain error</td>
    </tr>
    <tr>
        <td>
            type = luxury<br>
            plan = fixed_price
        </td>
        <td><a href="https://script.google.com/macros/s/AKfycbzp093GuTsC8jWmn6v8zWvaUAw0LErgD-P8VXt71Ll8-y2lewGh0Mw6sFyg6truO66T/exec?action=get&&service=calculatePrice&type=luxury&plan=fixed_price&distance=10&planned_distance=10&time=10&planned_time=10&inno_discount=no&email=v.safonov@innopolis.university">Request</a></td>
        <td>Invalid Request</td>
        <td>Invalid Request</td>
        <td>No error</td>
    </tr>
    <tr>
        <td>
            type = budget<br>
            plan = fixed_price<br>
            distance = 117<br>
            planned_distance = 100
        </td>
        <td><a href="https://script.google.com/macros/s/AKfycbzp093GuTsC8jWmn6v8zWvaUAw0LErgD-P8VXt71Ll8-y2lewGh0Mw6sFyg6truO66T/exec?action=get&&service=calculatePrice&type=budget&plan=fixed_price&distance=117&planned_distance=100&time=10&planned_time=10&inno_discount=no&email=v.safonov@innopolis.university">Request</a></td>
        <td>1400</td>
        <td>1250.0</td>
        <td>Computational error; behaves as if fixed price = 12.5</td>
    </tr>
    <tr>
        <td>
            type = budget<br>
            plan = fixed_price<br>
            distance = 117<br>
            planned_distance = 100<br>
            inno_discount = yes
        </td>
        <td><a href="https://script.google.com/macros/s/AKfycbzp093GuTsC8jWmn6v8zWvaUAw0LErgD-P8VXt71Ll8-y2lewGh0Mw6sFyg6truO66T/exec?action=get&&service=calculatePrice&type=budget&plan=fixed_price&distance=117&planned_distance=100&time=10&planned_time=10&inno_discount=yes&email=v.safonov@innopolis.university">Request</a></td>
        <td>1120</td>
        <td>1075.0</td>
        <td>Computational error; behaves as if fixed price = 12.5 and discount is 14%</td>
    </tr>
    <tr>
        <td>
            type = budget<br>
            plan = fixed_price<br>
            distance = 118<br>
            planned_distance = 100
        </td>
        <td><a href="https://script.google.com/macros/s/AKfycbzp093GuTsC8jWmn6v8zWvaUAw0LErgD-P8VXt71Ll8-y2lewGh0Mw6sFyg6truO66T/exec?action=get&&service=calculatePrice&type=budget&plan=fixed_price&distance=118&planned_distance=100&time=10&planned_time=10&inno_discount=no&email=v.safonov@innopolis.university">Request</a></td>
        <td>230</td>
        <td>166.6666</td>
        <td>Computational error; behaves as if price per minute is 16.666</td>
    </tr>
    <tr>
        <td>
            type = budget<br>
            plan = fixed_price<br>
            distance = 100<br>
            planned_distance = 100<br>
            time = 12<br>
            planned_time = 10
        </td>
        <td><a href="https://script.google.com/macros/s/AKfycbzp093GuTsC8jWmn6v8zWvaUAw0LErgD-P8VXt71Ll8-y2lewGh0Mw6sFyg6truO66T/exec?action=get&&service=calculatePrice&type=budget&plan=fixed_price&distance=100&planned_distance=100&time=12&planned_time=10&inno_discount=no&email=v.safonov@innopolis.university">Request</a></td>
        <td>276 (23*12)</td>
        <td>200</td>
        <td>Computational; behaves as if price per minute is 16.666</td>
    </tr>
    <tr>
        <td>
            type = budget<br>
            time < planned_time
        </td>
        <td><a href="https://script.google.com/macros/s/AKfycbzp093GuTsC8jWmn6v8zWvaUAw0LErgD-P8VXt71Ll8-y2lewGh0Mw6sFyg6truO66T/exec?action=get&&service=calculatePrice&type=budget&plan=minute&distance=10&planned_distance=10&time=5&planned_time=10&inno_discount=no&email=v.safonov@innopolis.university">Request</a></td>
        <td>115</td>
        <td>115</td>
        <td>No error</td>
    </tr>
    <tr>
        <td>
            type = budget<br>
            time > planned_time
        </td>
        <td><a href="https://script.google.com/macros/s/AKfycbzp093GuTsC8jWmn6v8zWvaUAw0LErgD-P8VXt71Ll8-y2lewGh0Mw6sFyg6truO66T/exec?action=get&&service=calculatePrice&type=budget&plan=minute&distance=10&planned_distance=10&time=20&planned_time=10&inno_discount=no&email=v.safonov@innopolis.university">Request</a></td>
        <td>460</td>
        <td>460</td>
        <td>No error</td>
    </tr>
    <tr>
        <td>
            type = luxury<br>
            distance < planned_distance
        </td>
        <td><a href="https://script.google.com/macros/s/AKfycbzp093GuTsC8jWmn6v8zWvaUAw0LErgD-P8VXt71Ll8-y2lewGh0Mw6sFyg6truO66T/exec?action=get&&service=calculatePrice&type=luxury&plan=minute&distance=5&planned_distance=10&time=10&planned_time=10&inno_discount=no&email=v.safonov@innopolis.university">Request</a></td>
        <td>570</td>
        <td>570</td>
        <td>No error</td>
    </tr>
    <tr>
        <td>
            type = luxury<br>
            distance > planned_distance
        </td>
        <td><a href="https://script.google.com/macros/s/AKfycbzp093GuTsC8jWmn6v8zWvaUAw0LErgD-P8VXt71Ll8-y2lewGh0Mw6sFyg6truO66T/exec?action=get&&service=calculatePrice&type=luxury&plan=minute&distance=15&planned_distance=10&time=10&planned_time=10&inno_discount=no&email=v.safonov@innopolis.university">Request</a></td>
        <td>570</td>
        <td>570</td>
        <td>No error</td>
    </tr>
    <tr>
        <td>
            type = budget<br>
            plan = minute<br>
            time = 10<br>
            inno_discount = yes
        </td>
        <td><a href="https://script.google.com/macros/s/AKfycbzp093GuTsC8jWmn6v8zWvaUAw0LErgD-P8VXt71Ll8-y2lewGh0Mw6sFyg6truO66T/exec?action=get&&service=calculatePrice&type=budget&plan=minute&distance=10&planned_distance=10&time=10&planned_time=10&inno_discount=yes&email=v.safonov@innopolis.university">Request</a></td>
        <td>184</td>
        <td>197.7999</td>
        <td>Computational error; discount applied is 14% instead of 20%</td>
    </tr>
</table>

## Summary

- The service does not handle planned and actual time and distance equal to 0 properly.
- It applies 14% discount instead of 20%.
- It counts a minute in budget costing 16.666 instead of 23 when the minute plan is enabled due to exceeded deviation.
- A fixed kilometer price in budget car is considered to cost 12.5. Applying a discount decreases it by 14%.
