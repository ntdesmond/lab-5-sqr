import asyncio

import aiohttp
import pytest
from main import get_expected_price, calculate_price, SpecKey

specs: dict[SpecKey, int] = {
    "budget_minute": 23,
    "luxury_minute": 57,
    "budget_fixed": 14,
    "max_deviation": 17,
    "inno_discount": 20,
}


@pytest.fixture(scope="session")
def event_loop():
    try:
        loop = asyncio.get_running_loop()
    except RuntimeError:
        loop = asyncio.new_event_loop()
    yield loop
    loop.close()


async def run_test(data: dict):
    async with aiohttp.ClientSession() as session:
        expected = get_expected_price(specs, **data)
        actual = await calculate_price(session, **data)
    assert actual == expected


@pytest.mark.asyncio
async def test_negative_distance():
    await run_test(
        {
            "car_type": "luxury",
            "plan": "minute",
            "distance": -10,
            "planned_distance": 10,
            "time": 10,
            "planned_time": 10,
            "is_discount_applied": False,
        },
    )


@pytest.mark.asyncio
async def test_zero_distance():
    await run_test(
        {
            "car_type": "luxury",
            "plan": "minute",
            "distance": 0,
            "planned_distance": 10,
            "time": 10,
            "planned_time": 10,
            "is_discount_applied": False,
        },
    )


@pytest.mark.asyncio
async def test_zero_distance_with_discount():
    await run_test(
        {
            "car_type": "luxury",
            "plan": "minute",
            "distance": 0,
            "planned_distance": 10,
            "time": 10,
            "planned_time": 10,
            "is_discount_applied": True,
        },
    )


@pytest.mark.asyncio
async def test_zero_distance_fixed_price():
    await run_test(
        {
            "car_type": "budget",
            "plan": "fixed_price",
            "distance": 0,
            "planned_distance": 10,
            "time": 10,
            "planned_time": 10,
            "is_discount_applied": False,
        },
    )


@pytest.mark.asyncio
async def test_negative_time():
    await run_test(
        {
            "car_type": "luxury",
            "plan": "minute",
            "distance": 10,
            "planned_distance": 10,
            "time": -10,
            "planned_time": 10,
            "is_discount_applied": False,
        },
    )


@pytest.mark.asyncio
async def test_zero_time():
    await run_test(
        {
            "car_type": "luxury",
            "plan": "minute",
            "distance": 10,
            "planned_distance": 10,
            "time": 0,
            "planned_time": 10,
            "is_discount_applied": False,
        },
    )


@pytest.mark.asyncio
async def test_zero_time_fixed_price():
    await run_test(
        {
            "car_type": "budget",
            "plan": "fixed_price",
            "distance": 10,
            "planned_distance": 10,
            "time": 0,
            "planned_time": 10,
            "is_discount_applied": False,
        },
    )


@pytest.mark.asyncio
async def test_zero_planned_distance():
    await run_test(
        {
            "car_type": "budget",
            "plan": "fixed_price",
            "distance": 10,
            "planned_distance": 0,
            "time": 10,
            "planned_time": 10,
            "is_discount_applied": False,
        },
    )


@pytest.mark.asyncio
async def test_zero_planned_time():
    await run_test(
        {
            "car_type": "luxury",
            "plan": "minute",
            "distance": 10,
            "planned_distance": 10,
            "time": 10,
            "planned_time": 0,
            "is_discount_applied": False,
        },
    )


@pytest.mark.asyncio
async def test_fixed_price_luxury():
    await run_test(
        {
            "car_type": "luxury",
            "plan": "fixed_price",
            "distance": 10,
            "planned_distance": 10,
            "time": 10,
            "planned_time": 10,
            "is_discount_applied": False,
        },
    )


@pytest.mark.asyncio
async def test_fixed_price_deviation_not_exceeded():
    await run_test(
        {
            "car_type": "budget",
            "plan": "fixed_price",
            "distance": 100 + 17,  # specs say max is 17% for me
            "planned_distance": 100,
            "time": 10,
            "planned_time": 10,
            "is_discount_applied": False,
        },
    )


@pytest.mark.asyncio
async def test_fixed_price_deviation_not_exceeded_with_discount():
    await run_test(
        {
            "car_type": "budget",
            "plan": "fixed_price",
            "distance": 100 + 17,  # specs say max is 17% for me
            "planned_distance": 100,
            "time": 10,
            "planned_time": 10,
            "is_discount_applied": True,
        },
    )


@pytest.mark.asyncio
async def test_fixed_price_distance_deviation_exceeded():
    await run_test(
        {
            "car_type": "budget",
            "plan": "fixed_price",
            "distance": 100 + 18,  # so here it's 17+1
            "planned_distance": 100,
            "time": 10,
            "planned_time": 10,
            "is_discount_applied": False,
        },
    )


@pytest.mark.asyncio
async def test_fixed_price_time_deviation_exceeded():
    await run_test(
        {
            "car_type": "budget",
            "plan": "fixed_price",
            "distance": 100,
            "planned_distance": 100,
            "time": 10 + 2,
            "planned_time": 10,
            "is_discount_applied": False,
        },
    )


@pytest.mark.asyncio
async def test_budget_minute_plan_less_time():
    await run_test(
        {
            "car_type": "budget",
            "plan": "minute",
            "distance": 10,
            "planned_distance": 10,
            "time": 5,
            "planned_time": 10,
            "is_discount_applied": False,
        },
    )


@pytest.mark.asyncio
async def test_budget_minute_plan_more_time():
    await run_test(
        {
            "car_type": "budget",
            "plan": "minute",
            "distance": 10,
            "planned_distance": 10,
            "time": 20,
            "planned_time": 10,
            "is_discount_applied": False,
        },
    )


@pytest.mark.asyncio
async def test_luxury_minute_plan_less_distance():
    await run_test(
        {
            "car_type": "luxury",
            "plan": "minute",
            "distance": 5,
            "planned_distance": 10,
            "time": 10,
            "planned_time": 10,
            "is_discount_applied": False,
        },
    )


@pytest.mark.asyncio
async def test_luxury_minute_plan_more_distance():
    await run_test(
        {
            "car_type": "luxury",
            "plan": "minute",
            "distance": 15,
            "planned_distance": 10,
            "time": 10,
            "planned_time": 10,
            "is_discount_applied": False,
        },
    )


@pytest.mark.asyncio
async def test_discount_applied():
    await run_test(
        {
            "car_type": "budget",
            "plan": "minute",
            "distance": 10,
            "planned_distance": 10,
            "time": 10,
            "planned_time": 10,
            "is_discount_applied": True,
        },
    )
