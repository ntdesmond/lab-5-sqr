import json
from typing import Literal, Any

import aiohttp
import asyncio

import settings
import test_data
from errors import InvalidRequestError

SpecKey = Literal[
    "budget_minute", "luxury_minute", "budget_fixed", "max_deviation", "inno_discount"
]
CarType = Literal["budget", "luxury"]
Plan = Literal["fixed_price", "minute"]

keys: dict[bytes, SpecKey] = {
    b"Budet car": "budget_minute",  # ne budet
    b"Luxury car": "luxury_minute",
    b"Fixed price": "budget_fixed",
    b"Allowed deviations": "max_deviation",
    b"Inno discount": "inno_discount",
}


async def get_specs(session: aiohttp.ClientSession) -> dict[SpecKey, int]:
    def line_to_key(line: bytes) -> SpecKey:
        return keys[next(marker for marker in keys if marker in line)]

    def line_to_value(line: bytes) -> int:
        return int(line.rsplit(b"=", maxsplit=1)[-1])

    async with session.get(
        f"{settings.base_url}?action=get&&service=getSpec&email={settings.email}"
    ) as response:
        # I sure do love parsing plain text responses!
        return {
            line_to_key(line): line_to_value(line)
            async for line in response.content
            if b"=" in line
        }


async def calculate_price(
    session: aiohttp.ClientSession,
    car_type: CarType,
    plan: Plan,
    distance: float,
    planned_distance: float,
    time: float,
    planned_time: float,
    is_discount_applied: bool,
) -> float | None:
    params = {
        "type": car_type,
        "plan": plan,
        "distance": distance,
        "planned_distance": planned_distance,
        "time": time,
        "planned_time": planned_time,
        "inno_discount": "yes" if is_discount_applied else "no",
    }
    param_string = "&".join(f"{key}={value}" for key, value in params.items())
    url = f"{settings.base_url}?action=get&&service=calculatePrice&{param_string}&email={settings.email}"
    async with session.get(url) as response:
        text = await response.text()
        print(f"{url=}\n{text=}")

        if text == "Invalid Request":
            return None

        return float(json.loads(text)["price"])


def get_expected_price(
    specs: dict[SpecKey, Any],
    car_type: CarType,
    plan: Plan,
    distance: float,
    planned_distance: float,
    time: float,
    planned_time: float,
    is_discount_applied: bool,
) -> float | None:
    def apply_discount(price: float):
        return (
            price * (100 - specs["inno_discount"]) / 100
            if is_discount_applied
            else price
        )

    def should_switch_to_minute_plan() -> bool:
        return (
            abs(distance / planned_distance * 100 - 100) > specs["max_deviation"]
            or abs(time / planned_time * 100 - 100) > specs["max_deviation"]
        )

    if (
        planned_time <= 0
        or planned_distance <= 0
        or distance <= 0
        or time <= 0
        or car_type == "luxury"
        and plan == "fixed_price"
    ):
        return None

    if plan == "fixed_price" and not should_switch_to_minute_plan():
        return apply_discount(planned_distance * specs["budget_fixed"])

    if car_type == "budget":
        return apply_discount(time * specs["budget_minute"])

    return apply_discount(time * specs["luxury_minute"])


async def test(session: aiohttp.ClientSession, specs: dict[SpecKey, Any]):
    async def run_test(test: dict[str, Any]):
        print(test)
        expected = get_expected_price(specs, **test)
        actual = await calculate_price(session, **test)
        equals = expected == actual
        print(f"{expected=}; {actual=}; {equals=}\n")

    for name, test in test_data.tests.items():
        print(f"{name} test")
        await run_test(test)


async def main():
    async with aiohttp.ClientSession() as session:
        specs = await get_specs(session)
        print("Specs:")
        print(*(f"{key}={value}" for key, value in specs.items()), sep="\n", end="\n\n")
        await test(session, specs)
    await asyncio.sleep(0.25)


if __name__ == "__main__":
    asyncio.run(main())
